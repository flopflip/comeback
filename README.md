CoMeBack software calculates Co-MEthylated regions (CMRs) based on microarray data like 450K or EPIC, 
as described in the pulblication "CoMeBack: DNA methylation array data analysis for co-methylated regions"

https://academic.oup.com/bioinformatics/article-abstract/36/9/2675/5716323?redirectedFrom=fulltext

Installation Instructions:

1. Download the comeback_X.tar.gz file
(To download the file, click the comeback file, a message "cannot show large file" will appear, 
then right-click on file and select "save-link-as".)

2. From within R, type install.packages(path_to_comeback_file, repos = NULL, type="source")


Note: installation was tested on Ubuntu Linux, OS X (El Cap+) and Windows 7,10.
  
Some windows 10 users may experience problems with the file auto-opening with Winzip - 
please download and install without unzipping. 

Windows 10 automatic updates are known to sporadically break different/arbitrary softwares -

As a free workaround for Windows 10 users, the installation was also tested in a free virtualbox Virtual machine with ubuntu linux:
install virtualbox from virtualbox.org, then install Lubuntu on a virtual machine, then install comeback as described above.

Basic Usage: 

See cmr.R file for examples

Advanced Usage:

??comeback
